#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    img.load("eyal.png");
    img.setImageType(OF_IMAGE_COLOR);

    ofSetBackgroundColor(0);
    ofSetColor(255,255,255);
    img.resize(800, 800);
    
    
}

//--------------------------------------------------------------
void ofApp::update(){

    
    ofBuffer buffer;
    ofSaveImage(img.getPixels(), buffer, OF_IMAGE_FORMAT_JPEG, OF_IMAGE_QUALITY_BEST);
    //cout << buffer.size() << endl;
    
    //ofSeedRandom(0);
    int whereToSkip = ofMap(ofRandom(0,ofGetWidth()), 0, ofGetWidth(), 0, buffer.size(), true);
    
    int left = buffer.size() - whereToSkip;
    int howMuchToSkip = ofMap(ofRandom(0,ofGetHeight()), 0, ofGetHeight(), 0, left*0.5, true);
    
    

    ofBuffer buffer2;
    for (int i = 0; i < buffer.size(); i++){
        if (i < whereToSkip ||
            i > (whereToSkip+howMuchToSkip)){
            buffer2.append(buffer.getData() + i, 1);
        } else {
             char * data = new char[1];
            data[0] = ofRandom(0,255);
            buffer2.append(data, 1);
            delete data;
            
        }
    }
    
    newImage.load(buffer2);
    
    
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){

    int spacingX = ofMap(ofSignedNoise(sin(ofGetElapsedTimef()*0.05))*100,-40,40,0,30);
    int spacingY = ofMap(ofSignedNoise(sin(ofGetElapsedTimef()*0.05))*100,-40,40,0,30);

//   int spacingX = 10;
//   int spacingY = 10;
    int gap = 30;
    float overlap = 0.5;
    
    
    cam.begin();
    cam.enableOrtho();
for (int j = 10;j < 800 ; j+=spacingY){
    float start = ofRandom(0,900);
    float atenuation = ofRandom(1,25);
    float oldV2 = 0; float oldV3 = 0;
    float base_brightness = ofRandom(-20,20);
    
        for (int i = 10;i < 800 ; i+=spacingX){
            ofColor color = newImage.getColor(i,j);

            float finaly;
            float deviation;
            float brightness = color.getBrightness();
            
            ofSetColor(color,base_brightness + brightness);
            
            float yPN = ofSignedNoise((i*0.07+ start)*0.1)*atenuation;
            
            float exponential_weight = ofMap(i,0,800,0,1)*ofMap(i,0,800,0,1);
            float perlinY = (1-exponential_weight)*j+ exponential_weight* (j +yPN);

            float b_weight = brightness *0.5;
            
            float y = perlinY;
            //(1-exponential_weight)*perlinY+ exponential_weight* (j + b_weight);
            //float z = (ofMap(i,0,800,0,1))*(ofMap(i,0,800,0,1)) * brightness;
            
            //z = (1-exponential_weight)*z+ exponential_weight* (4*yPN);
            float z = brightness*0.3;
            ofFill();
            ofBeginShape();
            ofVertex( i-spacingX/2-overlap-400, y-spacingY/2+gap-400, oldV2);
            ofVertex(i+spacingX/2+overlap-400, y-spacingY/2+gap-400, -z);
            ofVertex( i + spacingX/2+overlap-400, y+spacingY/2-gap-400, -z);
            ofVertex( i - spacingX/2-overlap-400, y+spacingY/2-gap-400, oldV3);
            oldV3 = -z;
            oldV2 = -z;
            

            
                ofEndShape();
        }
    }
    cam.end();
    newImage.update();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}


