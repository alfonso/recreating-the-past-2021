#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    color_container.push_back({213,52,34});
    color_container.push_back({251, 202, 122});
    color_container.push_back({255,194,6});
    color_container.push_back({246,135,54});
    color_container.push_back({247,169,176});
    color_container.push_back({245,133,45});
    color_container.push_back({41,29,39});
    color_container.push_back({23,88,153});
    color_container.push_back({0,154,78});


    
    
    //Colors:
//
//    Red = 213,52,34
//    Yellow = 249,216,13
//    DarkYellow = 255,194,6
//    Coral = 246,135,54
//    Pink =     247    169    176
//    Orange = 245,133,45
//    Blackish = 41,29,39
//    Blue = 23,88,153
//    Green = 0,154,78
    
    
    
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){

    
    ofSeedRandom(mouseX);
    for (int i = 0 ; i < 6; i++){
        for (int j = 0 ; j < 6; j++){
            
            if (i%2 == 0 and j%2==0){
            ofSetColor(249,216,13);
            int localx =i*100;
            int localy =j*100;
            
            ofDrawRectangle(localx, localy, 100, 100);
            int picked = ofRandom(8);
            ofSetColor(get<0>(color_container[picked]),get<1>(color_container[picked]),get<2>(color_container[picked]) );
            ofFill();
            ofBeginShape();
            ofVertex(localx, localy);
            ofVertex(localx + 100, localy  );
            ofVertex(localx + 100, localy + 100);
            ofVertex(localx + 20, localy + 80);
            ofEndShape(true);
            
            }
            
            if (i%2 == 1 and j%2==0){
            ofSetColor(249,216,13);
            int localx =i*100;
            int localy =j*100;
            
            ofDrawRectangle(localx, localy, 100, 100);
            int picked = ofRandom(9);
            ofSetColor(get<0>(color_container[picked]),get<1>(color_container[picked]),get<2>(color_container[picked]) );
            ofFill();
            ofBeginShape();
            ofVertex(localx+20, localy+20);
            ofVertex(localx + 100, localy  );
            ofVertex(localx + 100, localy + 100);
            ofVertex(localx , localy + 100);
            ofEndShape(true);
            
            }
            
            if (i%2 == 0 and j%2==1){
            ofSetColor(249,216,13);
            int localx =i*100;
            int localy =j*100;
            
            ofDrawRectangle(localx, localy, 100, 100);
            int picked = ofRandom(9);
            ofSetColor(get<0>(color_container[picked]),get<1>(color_container[picked]),get<2>(color_container[picked]) );
            ofFill();
            ofBeginShape();
            ofVertex(localx, localy);
            ofVertex(localx + 100, localy  );
            ofVertex(localx + 80, localy + 80);
            ofVertex(localx , localy + 100);
            ofEndShape(true);
            
            }
            
            if (i%2 == 1 and j%2==1){
            ofSetColor(249,216,13);
            int localx =i*100;
            int localy =j*100;
            
            ofDrawRectangle(localx, localy, 100, 100);
            int picked = ofRandom(9);
            ofSetColor(get<0>(color_container[picked]),get<1>(color_container[picked]),get<2>(color_container[picked]) );
            ofFill();
            ofBeginShape();
            ofVertex(localx, localy);
            ofVertex(localx + 80, localy +20 );
            ofVertex(localx + 100, localy + 100);
            ofVertex(localx , localy + 100);
            ofEndShape(true);
            
            }
            
        
    }
    
    
}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
