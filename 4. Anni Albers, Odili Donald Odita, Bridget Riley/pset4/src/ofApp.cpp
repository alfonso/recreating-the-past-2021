#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

}

//--------------------------------------------------------------
void ofApp::update(){

    ofSetCircleResolution(100);
    ofSetBackgroundColor(255);
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    float limit = 5;
    float mult = 0.5;
    float offset = mouseY*0.01;

    for (int x = 0; x < 30; x++){
        ofSetColor(218,37,39);
        ofDrawRectangle(10+x*20, 10, 10, 700);
    }
    
    for (int x = 0; x < 30; x++){
        
        if (x % 2 == 0){
        
        for (int y = 10; y < 710; y++) {
            //first green
            ofSetColor(41, 164, 64);
            ofDrawCircle(10 + x*20 +limit +  ofMap(sin(mouseX*0.05+y*0.01),-1,1,-limit, limit), y, 2+ofSignedNoise(y*0.002)*4);
            
            ofSetColor(5, 114, 181);
            ofDrawCircle(10 + x*20 +limit +  ofMap(-sin(mouseX*0.01+y*0.0081),-1,1,-limit, limit), y, 2+ofSignedNoise(y*0.002)*4);
        }
        }
        else{
            for (int y = 10; y < 710; y++) {
                //first green
                ofSetColor(41, 164, 64);
                ofDrawCircle(10 + x*20 +limit +  ofMap(-sin(mouseX*0.05+y*0.01),-1,1,-limit, limit), y, 2+ofSignedNoise(y*0.002)*4);
                
                ofSetColor(5, 114, 181);
                ofDrawCircle(10 + x*20 +limit +  ofMap(sin(mouseX*0.01+y*0.0081),-1,1,-limit, limit), y, 2+ofSignedNoise(y*0.002)*4);
            }
            
        }

        
        
    }
    
    for (int x = 0; x < 31; x++){
        ofSetColor(255);
        ofDrawRectangle(x*20, 10, 10, 700);
    }
    ofDrawRectangle(0,0,730,10);
    ofDrawRectangle(0,710,730,10);
    
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
