#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    
//    HERE I AM READING THE DIRECTORY WHERE I HAVE ALL MY PICTURE ALOCATED.
    
    ofDirectory dir;
    dir.listDir("pisa");
    for (int i = 0; i < dir.size(); i++){
        
        ofImage img; //generates an empty image
        images.push_back(img); //push it back to the list
        images.back().load(dir.getPath(i)); //actually now copypasting the image into that empty boi
        
        cout << "loaded " << i+1 << " of " << dir.size() << endl; //counting how many images I loaded
        
        
        
//        TIME HERE TO FIX AN ASPECT RATIO AND RESIZE IMAGES
        
        ofRectangle targetDim(0,0,420, 680); //Fixing aspect ratio
        ofRectangle imageDim(0,0,images.back().getWidth(),
                                 images.back().getHeight()); //Obtaining the square to resize to
        
        targetDim.scaleTo(imageDim); //Scaling it
        images.back().crop(targetDim.x, targetDim.y, targetDim.width, targetDim.height); //Croping but making it fit to the target rectangle
        images.back().resize(480, 640);
        
    }
    
    average.allocate(480, 640,OF_IMAGE_COLOR);
    for (int i = 0; i < 480 ; i++){
        for (int j = 0; j < 640;  j++) {
            
//            average
            float sumR = 0;
            float sumG = 0;
            float sumB = 0;
            
            for (int k = 0; k < images.size(); k++){
                ofColor  color = images[k].getColor(i,j);
                
                sumR += color.r;
                sumG += color.g;
                sumB += color.b;
                
            }
        
            sumR /= (float)images.size();
            sumG /= (float)images.size();
            sumB /= (float)images.size();
            average.setColor(i,j,ofColor(sumR, sumG, sumB));

        }
        
    }
    
    average.update();
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){

//    images[ ofGetFrameNum() % images.size() ].draw(0,0);
    average.draw(0,0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
