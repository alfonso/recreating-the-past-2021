#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

}



//--------------------------------------------------------------
void ofApp::update(){
}

//--------------------------------------------------------------
void ofApp::draw(){
    //Settings: Reproducing Vera Molnar Hommenage a Monet, 1983. Composed by 42*9
    //rectangles. Colors are: Blue(126,197,252) Pink(255,161,229)
    //Greenish brown(208,217,187) Brown(223,214,190)  Redish(246,175,173)
   
    //Defining now colors
    ofPoint Blue = {126,197,252};
    ofPoint Pink = {255,161,229};
    ofPoint BrownG = {208,217,187};
    ofPoint Brown = {223,214,190};
    ofPoint Redish = {246,175,173};
    
    //Defining Origin
    ofBackground(255,255,255);
    ofSetColor(0);

    ofSeedRandom(4);
    int step = 0;
    int size = 9*50;
    double values[size];
    for (int i=0; i<size; i++) {
        values[i]= ofRandom(0,1);
        
    }
    for (int i = 0; i<800; i+=100){
    for (int j = 0; j<800; j+=18) {
 
        float color = values[step];
    
        if (color < 0.5) {
            ofSetColor(208, 217, 187, 200);
        }
        else if (0.51 < color < 0.81){
            ofSetColor(223, 214, 190, 200);

        }
       else if (0.82< color <0.95){
            ofSetColor(255,161,229, 200);

        }
       else{
           ofSetColor(126,197,252, 200);

       }
        ofDrawRectangle(100+ i+ofSignedNoise(i+values[step]*0.1+j*0.0006, mouseX*0.1)*50,100+ j*ofRandom(0.978,0.999), 100, 18);
        step += 1;
        
        
    }

                //cout << i << "," << j << endl;
            }
            
}
   
    
    

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
