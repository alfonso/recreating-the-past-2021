#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    grabber.listDevices();
    grabber.setDeviceID(0);
    grabber.initGrabber(640,480);
    
    camGray.allocate(grabber.getWidth(),
                     grabber.getHeight(),
                     OF_IMAGE_GRAYSCALE);
    ofSetBackgroundColor(0);
    
    float x = 1300/4;
    float y = 0;
    fall.set(x,y);
    p1.set(0,100);
    p2.set(100,200);
}

//--------------------------------------------------------------
void ofApp::update(){
    grabber.update();
    if (grabber.isFrameNew()){
        convertColor(grabber, camGray, CV_RGB2GRAY);
        camGray.update();
        
        finder.findContours(grabber);
        
    }
    
    
    
    yp = (1300/4 - p1.x)*(p2.y - p1.y)/(p2.x-p1.x)+p1.y;
    
    
    if (fall.y > yp){
        fall.y += 0;
    }else {
        
    
    fall.y += 0.5;
    }
    if (fall.y >= 480){
        fall.y = 0;
    }
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    grabber.draw(0,0);
    ofTranslate(grabber.getWidth(), 0);
    vector < ofPoint > tops;

    
    for (int i = 0; i < finder.size(); i++){
        ofPolyline line = finder.getPolyline(i);
        line.close();
        line = line.getResampledBySpacing(5);
        line = line.getSmoothed(2);
        line.draw();
        ofMesh theMesh;
        ofTessellator tess;
        // **** convert poly to mesh ****
        tess.tessellateToMesh(line, ofPolyWindingMode::OF_POLY_WINDING_ODD, theMesh, true);
        ofSetColor(255,0,0);
        theMesh.draw();
    }
    
    for (int i = 0; i < finder.size(); i++){
        ofPolyline line = finder.getPolyline(i);
        ofPoint top;
        for (int i = 0; i< line.size(); i++){
            
            if (i == 0 ){
                top = line[i];
                
            } else if (line[i].y < top.y) {
                top = line[i];
            }
        }
        tops.push_back(top);
        
    }
    ofPoint  max ;
    ofPoint  smax ;
    
    
    for (int i = 0; i < tops.size(); i++){
        if (i == 0){
            max = tops[i];
        }else if (tops[i].y < max.y){
            max = tops[i];
            tops.erase(tops.begin()+i-1);
        }
    }
    
    for (int i = 0; i < tops.size(); i++){
        if (i == 0){
            smax = tops[i];
        }else if (tops[i].y < smax.y){
           smax = tops[i];
        }
    }
    
    p1 = max;
    p2 = smax;
    ofSetColor(0,255,0);
    ofDrawCircle(max, 3);
    ofSetColor(0,0,255);
    ofDrawCircle(smax, 3);
    ofSetColor(255,0,0);
    ofDrawLine(max,smax);
    
    ofSetColor(255,255,0);
    ofDrawCircle(fall.x, fall.y, 6);
    
    ofSetColor(255);

    ofDrawCircle(1300/4, yp, 6);

    

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
