#pragma once

#include "ofMain.h"

class stripe{
public:
    float xorig;
    float yorig;
    float angle;
    float radius;
    float i;
    ofColor color;
    void draw();
    stripe(); //constructor


};


class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();
    void launch(int xorig, int yorig);
        void keyPressed(int key);
        void keyReleased(int key);

    float last_update = 0;
    
    vector<stripe> stripes;
    float startTime;
    int countNr ;
    int size = 150;
    double values[];


};
