#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetBackgroundColor(0);
    ofSetLineWidth(3.2);
    
    ofSeedRandom(15);
    for (int n = 0; n < size; n++){

        values[n] = ofRandom(-50,50);
        cout << values[n] << endl ;

    }
   // int maxStripes;
    // maxStripes= 30;
    
   // for (int i = 0; i < maxStripes; i++){
        
   //     stripe newstripe;
     //   stripes.push_back(newstripe);
    
    
    float startTime = ofGetElapsedTimeMillis();
}



//--------------------------------------------------------------
void ofApp::update(){
    
    float t = ofGetElapsedTimef();
    
    


    
    if (t - last_update > 3*ofRandom(0.5,0.8)) {
         stripe newstripe;
        stripes.push_back(newstripe);
        
        last_update = t;
    }
    

  
}


void ofApp::draw(){
    for (int j = 0; j<stripes.size(); j++){
        
        stripes[j].draw();
        
    }
    
    
    
}
    
    

//--------------------------------------------------------------

void ofApp::launch(int xorig, int yorig){


}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}
//--------------------------------------------------------------


//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
stripe::stripe(){
    xorig = 400 + ofRandom(-350, 350);
    yorig = 400 + ofRandom(-350, 350);

    }



//--------------------------------------------------------------


void stripe::draw(){


        
    for (int i = 0 ; i < 70; i++){
        //float xorig = 400 + 200 * (sin(ofGetElapsedTimef()));
     //   float yorig = 400;
        float radius = 100 + i*3;
        float angle = ofMap(i,0,10,1,20) * ofMap(sin(ofGetElapsedTimef()*0.3), -1,1,0.01,0.05);

        float x = xorig + 200 * (sin(ofGetElapsedTimef()))+ radius*cos(angle*3);
        float y = yorig + radius*sin(angle*4);
        ofSetColor(200 , 252 - ofMap(i*0.1*sin(ofGetElapsedTimef()), -1, 1, 0, 40), 51);
        ofDrawLine(x,y-100, x, y+100);

    }
    }
