#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetBackgroundColor(0);
    ofSetLineWidth(3.2);
    float startTime = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
void ofApp::update(){

}


void ofApp::draw(){
    for (int countNr = 0; countNr < 6; countNr++) {
       float timer = ofGetElapsedTimeMillis() - startTime;
        if (timer >= 500) {
            countNr++;
            startTime = ofGetElapsedTimeMillis();

            launch(ofRandom(100,700),500);            }
    }
 
         }
    
    

//--------------------------------------------------------------

void ofApp::launch(int xorig, int yorig){
    for (int i = 0 ; i < 70; i++){
        //float xorig = 400 + 200 * (sin(ofGetElapsedTimef()));
     //   float yorig = 400;
        float radius = 100 + i*3;
        float angle = ofMap(i,0,10,1,20) * ofMap(sin(ofGetElapsedTimef()*0.3), -1,1,0.01,0.05);
        
        float x = xorig + 200 * (sin(ofGetElapsedTimef()))+ radius*cos(angle*3);
        float y = yorig + radius*sin(angle*4);
        ofSetColor(200+ofRandom(-55,55), 252 - ofMap(i*0.1*sin(ofGetElapsedTimef()), -1, 1, 0, 40), 51);
        ofDrawLine(x,y-100, x, y+100);
    }

}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}
//--------------------------------------------------------------


//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

