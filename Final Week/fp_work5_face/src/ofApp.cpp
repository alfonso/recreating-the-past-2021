#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    grabber.setup(640, 680);
    tracker.setup();
}

//--------------------------------------------------------------
void ofApp::update(){
    grabber.update();
    if (grabber.isFrameNew()) {
    
        tracker.update(grabber);
    }
    

}



//--------------------------------------------------------------
void ofApp::draw(){
    

    
    grabber.draw(0, 0);
   // tracker.drawDebug();
       if (tracker.size() > 0){
           ofPoint leftEye;
           ofPoint rightEye;
        
        ofPolyline leftEyePoly =tracker.getInstances()[0].getLandmarks().getImageFeature(ofxFaceTracker2Landmarks::LEFT_EYE);
        ofPolyline rightEyePoly =tracker.getInstances()[0].getLandmarks().getImageFeature(ofxFaceTracker2Landmarks::RIGHT_EYE);

        leftEye.set(0, 0);
        for (int i = 0; i < leftEyePoly.size(); i++ ){
            leftEye+=leftEyePoly[i];
        }
        leftEye /= (float)leftEyePoly.size();
        
        rightEye.set(0, 0);
        for (int i = 0; i < rightEyePoly.size(); i++ ){
            rightEye+=rightEyePoly[i];
        }
        rightEye /= (float)rightEyePoly.size();
        
        
        
        int eyedist = (rightEye.x - leftEye.x)/2;
           cout << eyedist << endl;
           ofPoint center;
           center.set((rightEye.x+leftEye.x)/2, (rightEye.y+leftEye.y)/2);
           ofDrawCircle((rightEye.x+leftEye.x)/2, (rightEye.y+leftEye.y)/2, 2);
           
           float spacing = (eyedist/ofMap(mouseY, -100,600, 1, 2.5));
           
          // ofDrawRectangle(center.x, center.y -eyedist*0.7, spacing, 1.4*eyedist);
           
           ofPoint p1;
           p1.set(center.x - 5 * spacing, center.y - eyedist*0.7);
           ofPoint p2;
           p2.set(p1.x + spacing, p1.y);
           ofPoint p3;
           p3.set(p1.x + 2*spacing, p1.y);
           ofPoint p4;
           p4.set(p1.x + 3*spacing, p1.y);
           ofPoint p5;
           p5.set(p1.x + 4*spacing, p1.y);
           ofPoint p6;
           p6.set(p1.x + 5*spacing, p1.y);
           ofPoint p7;
           p7.set(p1.x + 6*spacing, p1.y);
           ofPoint p8;
           p8.set(p1.x + 7*spacing, p1.y);
           ofPoint p9;
           p9.set(p1.x + 8*spacing, p1.y);
           ofPoint p10;
           p10.set(p1.x + 9*spacing, p1.y);
           ofPoint p11;
           p11.set(p1.x + 10*spacing, p1.y);
           
           ofPoint p12;
           p12.set(center.x - 5 * spacing, center.y + eyedist*0.7);
           ofPoint p13;
           p13.set(p12.x + spacing, p12.y);
           ofPoint p14;
           p14.set(p12.x + 2*spacing, p12.y);
           ofPoint p15;
           p15.set(p12.x + 3*spacing, p12.y);
           ofPoint p16;
           p16.set(p12.x + 4*spacing, p12.y);
           ofPoint p17;
           p17.set(p12.x + 5*spacing, p12.y);
           ofPoint p18;
           p18.set(p12.x + 6*spacing, p12.y);
           ofPoint p19;
           p19.set(p12.x + 7*spacing, p12.y);
           ofPoint p20;
           p20.set(p12.x + 8*spacing, p12.y);
           ofPoint p21;
           p21.set(p12.x + 9*spacing, p12.y);
           ofPoint p22;
           p22.set(p12.x + 10*spacing, p12.y);
           
           
           
           ofMesh m1;
            m1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m1.addVertex(ofPoint(p1));
           m1.addVertex(ofPoint(p2));
           m1.addVertex(ofPoint(p12));
           m1.addVertex(ofPoint(p13));
           
           m1.addTexCoord( grabber.getTexture().getCoordFromPoint(p6.x, p6.y));
           m1.addTexCoord( grabber.getTexture().getCoordFromPoint(p7.x, p7.y));
           m1.addTexCoord( grabber.getTexture().getCoordFromPoint(p17.x, p18.y));
           m1.addTexCoord( grabber.getTexture().getCoordFromPoint(p18.x, p19.y));
           
           ofMesh m2;
            m2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m2.addVertex(ofPoint(p2));
           m2.addVertex(ofPoint(p3));
           m2.addVertex(ofPoint(p13));
           m2.addVertex(ofPoint(p14));
           
           m2.addTexCoord( grabber.getTexture().getCoordFromPoint(p1.x, p1.y));
           m2.addTexCoord( grabber.getTexture().getCoordFromPoint(p2.x, p2.y));
           m2.addTexCoord( grabber.getTexture().getCoordFromPoint(p12.x, p12.y));
           m2.addTexCoord( grabber.getTexture().getCoordFromPoint(p13.x, p13.y));
           
           ofMesh m3;
            m3.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m3.addVertex(ofPoint(p3));
           m3.addVertex(ofPoint(p4));
           m3.addVertex(ofPoint(p14));
           m3.addVertex(ofPoint(p15));
           
           m3.addTexCoord( grabber.getTexture().getCoordFromPoint(p2.x, p2.y));
           m3.addTexCoord( grabber.getTexture().getCoordFromPoint(p3.x, p3.y));
           m3.addTexCoord( grabber.getTexture().getCoordFromPoint(p13.x, p13.y));
           m3.addTexCoord( grabber.getTexture().getCoordFromPoint(p14.x, p14.y));
           
           ofMesh m4;
            m4.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m4.addVertex(ofPoint(p4));
           m4.addVertex(ofPoint(p5));
           m4.addVertex(ofPoint(p15));
           m4.addVertex(ofPoint(p16));
           
           m4.addTexCoord( grabber.getTexture().getCoordFromPoint(p7.x, p7.y));
           m4.addTexCoord( grabber.getTexture().getCoordFromPoint(p8.x, p8.y));
           m4.addTexCoord( grabber.getTexture().getCoordFromPoint(p18.x, p18.y));
           m4.addTexCoord( grabber.getTexture().getCoordFromPoint(p19.x, p19.y));
           
           
           ofMesh m5;
            m5.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m5.addVertex(ofPoint(p5));
           m5.addVertex(ofPoint(p6));
           m5.addVertex(ofPoint(p16));
           m5.addVertex(ofPoint(p17));
           
           m5.addTexCoord( grabber.getTexture().getCoordFromPoint(p3.x, p3.y));
           m5.addTexCoord( grabber.getTexture().getCoordFromPoint(p4.x, p4.y));
           m5.addTexCoord( grabber.getTexture().getCoordFromPoint(p14.x, p14.y));
           m5.addTexCoord( grabber.getTexture().getCoordFromPoint(p15.x, p15.y));
           
           
           ofMesh m6;
            m6.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m6.addVertex(ofPoint(p6));
           m6.addVertex(ofPoint(p7));
           m6.addVertex(ofPoint(p17));
           m6.addVertex(ofPoint(p18));
           
           m6.addTexCoord( grabber.getTexture().getCoordFromPoint(p8.x, p8.y));
           m6.addTexCoord( grabber.getTexture().getCoordFromPoint(p9.x, p9.y));
           m6.addTexCoord( grabber.getTexture().getCoordFromPoint(p19.x, p19.y));
           m6.addTexCoord( grabber.getTexture().getCoordFromPoint(p20.x, p20.y));
           
           
           ofMesh m7;
            m7.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m7.addVertex(ofPoint(p7));
           m7.addVertex(ofPoint(p8));
           m7.addVertex(ofPoint(p18));
           m7.addVertex(ofPoint(p19));
           
           m7.addTexCoord( grabber.getTexture().getCoordFromPoint(p4.x, p4.y));
           m7.addTexCoord( grabber.getTexture().getCoordFromPoint(p5.x, p5.y));
           m7.addTexCoord( grabber.getTexture().getCoordFromPoint(p15.x, p15.y));
           m7.addTexCoord( grabber.getTexture().getCoordFromPoint(p16.x, p16.y));
           
           ofMesh m8;
            m8.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m8.addVertex(ofPoint(p8));
           m8.addVertex(ofPoint(p9));
           m8.addVertex(ofPoint(p19));
           m8.addVertex(ofPoint(p20));
           
           m8.addTexCoord( grabber.getTexture().getCoordFromPoint(p9.x, p9.y));
           m8.addTexCoord( grabber.getTexture().getCoordFromPoint(p10.x, p10.y));
           m8.addTexCoord( grabber.getTexture().getCoordFromPoint(p20.x, p20.y));
           m8.addTexCoord( grabber.getTexture().getCoordFromPoint(p21.x, p21.y));
           
           ofMesh m9;
            m9.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m9.addVertex(ofPoint(p9));
           m9.addVertex(ofPoint(p10));
           m9.addVertex(ofPoint(p20));
           m9.addVertex(ofPoint(p21));
           
           m9.addTexCoord( grabber.getTexture().getCoordFromPoint(p10.x, p10.y));
           m9.addTexCoord( grabber.getTexture().getCoordFromPoint(p11.x, p11.y));
           m9.addTexCoord( grabber.getTexture().getCoordFromPoint(p21.x, p21.y));
           m9.addTexCoord( grabber.getTexture().getCoordFromPoint(p22.x, p22.y));
           
           ofMesh m10;
            m10.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           m10.addVertex(ofPoint(p9));
           m10.addVertex(ofPoint(p10));
           m10.addVertex(ofPoint(p20));
           m10.addVertex(ofPoint(p21));
           
           m10.addTexCoord( grabber.getTexture().getCoordFromPoint(p5.x, p5.y));
           m10.addTexCoord( grabber.getTexture().getCoordFromPoint(p6.x, p6.y));
           m10.addTexCoord( grabber.getTexture().getCoordFromPoint(p16.x, p16.y));
           m10.addTexCoord( grabber.getTexture().getCoordFromPoint(p17.x, p17.y));
           

                   grabber.getTexture().bind();
           
                   m1.draw();
           m2.draw();
           m3.draw();
           m4.draw();
           m5.draw();
           m6.draw();
           m7.draw();
           m8.draw();
           m9.draw();
           m10.draw();
           
                   grabber.getTexture().unbind();


           
}

}
