#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    grabber.setup(1200, 1200);

}

//--------------------------------------------------------------
void ofApp::update(){

    grabber.update();
    
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    grabber.draw(0,0);
    
    int seed = 2;
    int step = (int) ofMap(mouseY, -100,1000, 1,20) ;
    
    for (int i = 00 ; i < 25 ; i++){
        
        ofMesh mesh1;
        ofMesh mesh2;
        mesh1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
        mesh2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
        
            mesh1.addVertex(ofPoint(300,grabber.getHeight()/2+ i*step));
            mesh1.addVertex(ofPoint(900,grabber.getHeight()/2+ i*step));
            mesh1.addVertex(ofPoint(300,grabber.getHeight()/2 + i*step+ step));
            mesh1.addVertex(ofPoint(900,grabber.getHeight()/2 + i*step+ step));
        
            mesh2.addVertex(ofPoint(300,1*i*step));
            mesh2.addVertex(ofPoint(900,1*i*step));
            mesh2.addVertex(ofPoint(300,1*i*step+step));
            mesh2.addVertex(ofPoint(900,1*i*step+step));
 

            
            
            mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(300,2*i*step));
            mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(900,2*i*step));
            mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(300,2*i*step+step));
            mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(900,2*i*step+step));
        
            
            mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(300,2*i*step + step));
            mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(900,2*i*step + step));
            mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(300,2*i*step+2*step));
            mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(900,2*i*step+2*step));
            
            

            

        
            grabber.getTexture().bind();
            mesh1.draw();
        mesh2.draw();
            grabber.getTexture().unbind();
            
    }
    
    
//    ofMesh mesh1;
//    mesh1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
//    mesh1.addVertex(ofPoint(0,0));
//    mesh1.addVertex(ofPoint(600,0));
//    mesh1.addVertex(ofPoint(600,600));
//    mesh1.addVertex(ofPoint(0,600));
//
//    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(0,0));
//    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(700,0));
//    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(700,700));
//    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(0,700));
//
//
//
//
//    grabber.getTexture().bind();
//    mesh1.draw();
//    grabber.getTexture().unbind();

    ofSetColor(255);
    ofDrawRectangle(0, 0, 300, 1200);
    ofDrawRectangle(900, 0, 300, 1200);


}

//--------------------------------------------------------------

