#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    img.load("pr4.png");
    img.setImageType(OF_IMAGE_COLOR);
    img.resize(2944/1.8,1952/1.8);
    ofSetBackgroundColor(255, 255, 255);


}

//--------------------------------------------------------------
void ofApp::update(){
    originX = mouseX;
    originY = mouseY;
    
    p1x = originX + 200;
    p1y = originY + 200;
    
    p2x = p1x + 50;
    p2y = p1y - 20;
    
    p3x = p2x + 60;
    p3y = p2y;
    
    p4x = p3x + 50;
    p4y = p3y + 20;
    
    p5x = p2x;
    p5y = p2y + 40;
    
    p6x = p5x + 60;
    p6y = p5y;
    
    p7x = p1x;
    p7y = p1y + 550;
    
    
    p11x = p2x;
    p11y = p2y + 550;
    
    p12x = p3x;
    p12y = p3y + 550;
    
    p10x = p4x;
    p10y = p4y + 550;
    
    p8x = p5x;
    p8y = p5y + 550 ;
    
    p9x = p6x;
    p9y = p6y + 550 ;
    
    
    
    img.update();


}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255);
    img.draw(0,0);

    
//
////    L1-2
//    ofDrawLine(p1x, p1y, p2x, p2y);
//
////    L2-3
//    ofDrawLine(p2x, p2y, p3x, p3y);
//
////    L3-4
//    ofDrawLine(p3x, p3y, p4x, p4y);
//
////    L1-5
//    ofDrawLine(p1x, p1y, p5x, p5y);
//
////    L5-6
//    ofDrawLine(p5x, p5y, p6x, p6y);
//
////    L6-4
//    ofDrawLine(p6x, p6y, p4x, p4y);
//
////    L1-7
//    ofDrawLine(p1x, p1y, p7x, p7y);
//
////    L7-11
//    ofDrawLine(p7x, p7y, p11x, p11y);
//
////    L7-8
//    ofDrawLine(p7x, p7y, p8x, p8y);
//
////    L8-9
//    ofDrawLine(p8x, p8y, p9x, p9y);
//
////    L9-10
//    ofDrawLine(p9x, p9y, p10x, p10y);
//
////    L11-12
//    ofDrawLine(p11x, p11y, p12x, p12y);
    
    
    ofMesh m1;
    m1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m1.addVertex(ofPoint(p6x, p6y));
    m1.addVertex(ofPoint(p4x, p4y));
    m1.addVertex(ofPoint(p9x, p9y));
    m1.addVertex(ofPoint(p10x, p10y));

    m1.addTexCoord( img.getTexture().getCoordFromPoint(p1x,p1y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p2x,p2y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p7x,p7y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p11x,p11y));
    

    

    ofMesh m2;
    m2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m2.addVertex(ofPoint(p5x, p5y));
    m2.addVertex(ofPoint(p6x, p6y));
    m2.addVertex(ofPoint(p8x, p8y));
    m2.addVertex(ofPoint(p9x, p9y));

    m2.addTexCoord( img.getTexture().getCoordFromPoint(p2x,p2y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p3x,p3y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p11x,p11y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p12x,p12y));


    
    ofMesh m3;
    m3.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m3.addVertex(ofPoint(p1x, p1y));
    m3.addVertex(ofPoint(p5x, p5y));
    m3.addVertex(ofPoint(p7x, p7y));
    m3.addVertex(ofPoint(p8x, p8y));

    
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p3x,p3y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p4x,p4y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p12x,p12y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p10x,p10y));



    ofMesh m4;
    m4.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m4.addVertex(ofPoint(p1x, p1y));
    m4.addVertex(ofPoint(p2x, p2y));
    m4.addVertex(ofPoint(p5x, p5y));
    m4.addVertex(ofPoint(p3x, p3y));
    m4.addVertex(ofPoint(p6x, p6y));
    m4.addVertex(ofPoint(p4x, p4y));
    
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p7x,p7y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p11x,p11y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p8x,p8y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p12x,p12y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p9x,p9y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p10x,p10y));

    img.getTexture().bind();
    m1.draw();
    m2.draw();
    m3.draw();
    m4.draw();
    img.getTexture().unbind();


 
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
