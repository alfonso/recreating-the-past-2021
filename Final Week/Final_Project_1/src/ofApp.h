#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    float d1 ;
    float f1 ;
    float p1 ;
    float d2 ;
    float f2 ;
    float p2 ;
    
    float d3 ;
    float f3 ;
    float p3 ;
    float d4 ;
    float f4 ;
    float p4 ;
    ofPolyline trail;

};
