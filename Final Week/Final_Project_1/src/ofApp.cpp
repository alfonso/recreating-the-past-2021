#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    ofSetBackgroundColor(255);
    
    f1 = 10;
    f2 = 3;
    f3 = 1;
    f4 = 2;
    
    d1 = 0.039;
    d2 = 0.006;
    d3 = 0;
    d4 = 0.0045;
    
    p1 = 0;
    p2 = 0;
    p3 = 3.14159/2;
    p4 = 0;
    
    ofSetColor(0);
    
}

//--------------------------------------------------------------
void ofApp::update(){
    float x = 400 +100 *(exp(-d1*ofGetElapsedTimef()) * sin (f1 * ofGetElapsedTimef() + p1) +
              exp(-d2*ofGetElapsedTimef()) * sin (f2 * ofGetElapsedTimef() + p2)) ;
    
    float y = 400 +100*(exp(-d3*ofGetElapsedTimef()) * sin (f3 * ofGetElapsedTimef() + p3) +
              exp(-d4*ofGetElapsedTimef()) * sin (f4 * ofGetElapsedTimef() + p4)) ;
    trail.addVertex(x,y);
//    cout << x << "   " << y << endl;
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    trail.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
