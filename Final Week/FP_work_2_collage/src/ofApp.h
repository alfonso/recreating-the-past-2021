#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
    
    int p1x;
    int p1y;
    
    int p2x;
    int p2y;
    
    int p3x;
    int p3y;
    
    int p4x;
    int p4y;
    
    int p5x;
    int p5y;
    
    int p6x;
    int p6y;
    
    int p7x;
    int p7y;
    
    int p8x;
    int p8y;
    
    int p9x;
    int p9y;
    
    int p10x;
    int p10y;
    
    int p11x;
    int p11y;
    
    int p12x;
    int p12y;
        
    int originX ;
    int originY ;
    
    ofImage img;
};
