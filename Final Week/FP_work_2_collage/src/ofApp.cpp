#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    img.load("pr6.png");
    img.setImageType(OF_IMAGE_COLOR);
    img.resize(2944/1.8,1952/1.8);
    ofSetBackgroundColor(255, 255, 255);
    ofSeedRandom(15);


}

//--------------------------------------------------------------
void ofApp::update(){
    originX = mouseX;
    originY = mouseY;
    
    int height = 550;
    int w1 = 50 ;
    int w2 = 60 ;
    int h = 20;

    
    p1x = originX + 0;
    p1y = originY + 0;
    
    p2x = p1x + w1;
    p2y = p1y - h;
    
    p3x = p2x + w2;
    p3y = p2y;
    
    p4x = p3x + w1;
    p4y = p3y + h;
    
    p5x = p2x;
    p5y = p2y + 2*h;
    
    p6x = p5x + w2;
    p6y = p5y;
    
    p7x = p1x;
    p7y = p1y + height;
    
    
    p11x = p2x;
    p11y = p2y + height;
    
    p12x = p3x;
    p12y = p3y + height;
    
    p10x = p4x;
    p10y = p4y + height;
    
    p8x = p5x;
    p8y = p5y + height ;
    
    p9x = p6x;
    p9y = p6y + height ;
    
    
    
    img.update();


}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255);
    img.draw(0,0);

    
//
////    L1-2
//    ofDrawLine(p1x, p1y, p2x, p2y);
//
////    L2-3
//    ofDrawLine(p2x, p2y, p3x, p3y);
//
////    L3-4
//    ofDrawLine(p3x, p3y, p4x, p4y);
//
////    L1-5
//    ofDrawLine(p1x, p1y, p5x, p5y);
//
////    L5-6
//    ofDrawLine(p5x, p5y, p6x, p6y);
//
////    L6-4
//    ofDrawLine(p6x, p6y, p4x, p4y);
//
////    L1-7
//    ofDrawLine(p1x, p1y, p7x, p7y);
//
////    L7-11
//    ofDrawLine(p7x, p7y, p11x, p11y);
//
////    L7-8
//    ofDrawLine(p7x, p7y, p8x, p8y);
//
////    L8-9
//    ofDrawLine(p8x, p8y, p9x, p9y);
//
////    L9-10
//    ofDrawLine(p9x, p9y, p10x, p10y);
//
////    L11-12
//    ofDrawLine(p11x, p11y, p12x, p12y);
    
    ofSeedRandom(15);

    for( int i = 0; i < 1200; i+=160){

        int j = ofRandom(-100, 100);
        i = i + ofRandom(20);
    ofMesh m1;
    m1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m1.addVertex(ofPoint(p6x+i, p6y+j));
    m1.addVertex(ofPoint(p4x+i, p4y+j));
    m1.addVertex(ofPoint(p9x+i, p9y+j));
    m1.addVertex(ofPoint(p10x+i, p10y+j));

    m1.addTexCoord( img.getTexture().getCoordFromPoint(p1x+i,p1y+j));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p2x+i,p2y+j));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p7x+i,p7y+j));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p11x+i,p11y+j));
    

    

    ofMesh m2;
    m2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m2.addVertex(ofPoint(p5x+i, p5y+j));
    m2.addVertex(ofPoint(p6x+i, p6y+j));
    m2.addVertex(ofPoint(p8x+i, p8y+j));
    m2.addVertex(ofPoint(p9x+i, p9y+j));

    m2.addTexCoord( img.getTexture().getCoordFromPoint(p2x+i,p2y+j));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p3x+i,p3y+j));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p11x+i,p11y+j));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p12x+i,p12y+j));


    
    ofMesh m3;
    m3.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m3.addVertex(ofPoint(p1x+i, p1y+j));
    m3.addVertex(ofPoint(p5x+i, p5y+j));
    m3.addVertex(ofPoint(p7x+i, p7y+j));
    m3.addVertex(ofPoint(p8x+i, p8y+j));

    
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p3x+i,p3y+j));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p4x+i,p4y+j));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p12x+i,p12y+j));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p10x+i,p10y+j));



    ofMesh m4;
    m4.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    m4.addVertex(ofPoint(p1x+i, p1y+j));
    m4.addVertex(ofPoint(p2x+i, p2y+j));
    m4.addVertex(ofPoint(p5x+i, p5y+j));
    m4.addVertex(ofPoint(p3x+i, p3y+j));
    m4.addVertex(ofPoint(p6x+i, p6y+j));
    m4.addVertex(ofPoint(p4x+i, p4y+j));
    
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p7x+i,p7y+j));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p11x+i,p11y+j));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p8x+i,p8y+j));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p12x+i,p12y+j));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p9x+i,p9y+j));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p10x+i,p10y+j));

    img.getTexture().bind();
    m1.draw();
    m2.draw();
    m3.draw();
    m4.draw();
    img.getTexture().unbind();

    }

    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

