#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    grabber.setup(640, 680);
    tracker.setup();
}

//--------------------------------------------------------------
void ofApp::update(){

    
    grabber.update();
    if (grabber.isFrameNew()) {
    
        tracker.update(grabber);
    }
    

}

//--------------------------------------------------------------
void ofApp::draw(){
    
    grabber.draw(0, 0);
    //tracker.drawDebug();
       if (tracker.size() > 0){
        
        ofPolyline leftEyePoly =tracker.getInstances()[0].getLandmarks().getImageFeature(ofxFaceTracker2Landmarks::LEFT_EYE);
        ofPolyline rightEyePoly =tracker.getInstances()[0].getLandmarks().getImageFeature(ofxFaceTracker2Landmarks::RIGHT_EYE);

        leftEye.set(0, 0);
        for (int i = 0; i < leftEyePoly.size(); i++ ){
            leftEye+=leftEyePoly[i];
        }
        leftEye /= (float)leftEyePoly.size();
        
        rightEye.set(0, 0);
        for (int i = 0; i < rightEyePoly.size(); i++ ){
            rightEye+=rightEyePoly[i];
        }
        rightEye /= (float)rightEyePoly.size();
        
        
        
        int eyedist = ofDist(leftEye[0],rightEye[0], leftEye[1],rightEye[1]);
        
        
           
           
         // Now with the eye info, I draw two triangles, one on each eye. Left triangle is composed by p1 p2 and p3. Right triangle is composed by p4 p5 and p6
        ofPoint t1;
        t1.set(eyedist*0.17,eyedist*0.58);
        t1 = leftEye -t1;
    
        
        ofPoint t2;
        t2.set(eyedist*0.17,-eyedist*0.58);
        t2 = leftEye +t2;
     
        
        ofPoint t3;
        t3.set(0,eyedist*0.4);
        t3 = leftEye +t3;
        

        
        ofPoint t4;
        t4.set(eyedist*0.17,eyedist*0.58);
        t4 = rightEye -t4;
    
        
        ofPoint t5;
        t5.set(eyedist*0.17,-eyedist*0.58);
        t5 = rightEye +t5;
     
        
        ofPoint t6;
        t6.set(0,eyedist*0.4);
        t6 = rightEye +t6;
        
        //Using the triangle points to draw the meshes.
//        ofMesh mesh1;
//        mesh1.setMode(OF_PRIMITIVE_TRIANGLES);
//        mesh1.addVertex(ofPoint(t1));
//        mesh1.addVertex(ofPoint(t2));
//        mesh1.addVertex(ofPoint(t3));
//
//        mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t4.x, t4.y));
//        mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t5.x, t5.y));
//        mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t6.x, t6.y));
//
//       ofMesh mesh2;
//       mesh2.setMode(OF_PRIMITIVE_TRIANGLES);
//       mesh2.addVertex(ofPoint(t4));
//       mesh2.addVertex(ofPoint(t5));
//       mesh2.addVertex(ofPoint(t6));
//
//       mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t1.x, t1.y));
//       mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t2.x, t2.y));
//       mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t3.x, t3.y));
//
//        grabber.getTexture().bind();
//
//        mesh1.draw();
//        mesh2.draw();
//
//        grabber.getTexture().unbind();
           
           ofPoint t7;
           t7.set(rightEye.x, rightEye.y-eyedist*0.35 + eyedist*0.15);

           ofPoint t8;
           t8.set(rightEye.x- eyedist*0.2, rightEye.y+ eyedist*0.15);
           
           ofPoint t9;
           t9.set(rightEye.x+ eyedist*0.2, rightEye.y+ eyedist*0.15);
           
           ofPoint t10;
           t10.set((rightEye.x + leftEye.x)/2, (rightEye.y + leftEye.y)/2- eyedist*0.35-0.7*eyedist);
   

           ofPoint t11;
           t11.set((rightEye.x + leftEye.x)/2 - eyedist*0.2, (rightEye.y + leftEye.y)/2-0.7*eyedist);

           
           ofPoint t12;
           t12.set((rightEye.x + leftEye.x)/2+ eyedist*0.2, (rightEye.y + leftEye.y)/2-0.7*eyedist);
           
           
           
                   ofMesh mesh1;
                   mesh1.setMode(OF_PRIMITIVE_TRIANGLES);
                   mesh1.addVertex(ofPoint(t7));
                   mesh1.addVertex(ofPoint(t8));
                   mesh1.addVertex(ofPoint(t9));
           
                   mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t10.x, t10.y));
                   mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t11.x, t11.y));
                   mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(t12.x, t12.y));
           
                  ofMesh mesh2;
                  mesh2.setMode(OF_PRIMITIVE_TRIANGLES);
                  mesh2.addVertex(ofPoint(t10));
                  mesh2.addVertex(ofPoint(t11));
                  mesh2.addVertex(ofPoint(t12));
           
                  mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t7.x, t7.y));
                  mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t8.x, t8.y));
                  mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(t9.x, t9.y));
           
                   grabber.getTexture().bind();
           
                   mesh1.draw();
                   mesh2.draw();
           
                   grabber.getTexture().unbind();
        
        
        
   }

}

//--------------------------------------------------------------

