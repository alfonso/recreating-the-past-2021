#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    grabber.setup(600, 600);

}

//--------------------------------------------------------------
void ofApp::update(){

    grabber.update();
    
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofMesh mesh1;
    mesh1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    mesh1.addVertex(ofPoint(0,0));
    mesh1.addVertex(ofPoint(600,0));
    mesh1.addVertex(ofPoint(600,600));
    mesh1.addVertex(ofPoint(0,600));
    
    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(0,0));
    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(600,0));
    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(600,600));
    mesh1.addTexCoord( grabber.getTexture().getCoordFromPoint(0,600));

    
    ofMesh mesh2;
    mesh2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    mesh2.addVertex(ofPoint(0,600));
    mesh2.addVertex(ofPoint(600,600));
    mesh2.addVertex(ofPoint(1200,600));
    mesh2.addVertex(ofPoint(0,1200));
    
    mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(0,0));
    mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(600,0));
    mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(600,600));
    mesh2.addTexCoord( grabber.getTexture().getCoordFromPoint(0,600));

    
    grabber.getTexture().bind();
    mesh1.draw();
    mesh2.draw();
    grabber.getTexture().unbind();



}

//--------------------------------------------------------------
