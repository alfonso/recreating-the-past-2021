#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    img.load("pr4.png");
    img.setImageType(OF_IMAGE_COLOR);
    img.resize(2944/1.8,1952/1.8);
    ofSetBackgroundColor(255, 255, 255);
}

//--------------------------------------------------------------
void ofApp::update(){
    int origx = mouseX;
    int origy = mouseY;
    
    int hs = 20;
    int w = 50;
    int h = 300;
    
    
    p26.set(origx,origy + hs);
    p1.set(p26.x ,p26.y + h);
    p25.set(origx + w, origy);
    p24.set(p25.x, p25.y+hs);
    p2.set(p25.x, p25.y+h);
    p3.set(p25.x, p24.y+h);
    p23.set(p25.x + w, origy);
    p4.set(p23.x, p23.y + h + hs);
    
    p22.set(p24.x + 2*w, p23.y + hs);
    p5.set( p22.x,p22.y + h );
    p21.set(p22.x + w , p23.y );
    p20.set( p21.x, p21.y + hs);
    p7p.set( p21.x, p21.y +h);
    p6.set(p7p.x , p7p.y + hs);
    p19.set( p21.x + w,p21.y );
    p7.set( p19.x,p19.y + h );
    p18.set(p19.x,p19.y + hs);
    
    p17.set(p19.x + w, p19.y);
    p16.set(p17.x, p17.y + hs);
    p8.set(p16.x, p16.y + h);
    p15.set(p17.x + w , p17.y);
    p14.set(p15.x, p15.y + hs);
    p9.set(p14.x, p14.y + h);
    p13.set(p15.x + w, p15.y);
    p12.set(p13.x , p13.y + hs);
    p10.set(p13.x , p13.y + h);
    p11.set(p13.x + w, p13.y);
    
    img.update();

    

}

//--------------------------------------------------------------
void ofApp::draw(){
   
    img.draw(0,0);
    
//    ofDrawLine(p26.x, p26.y,p25.x,  p25.y);
//    ofDrawLine(p26.x,  p26.y,p24.x, p24.y);
//    ofDrawLine(p26.x,p26.y, p1.x,  p1.y);
//    ofDrawLine(p2.x,  p2.y, p1.x,p1.y);
//    ofDrawLine(p2.x,p2.y, p3.x,  p3.y);
//    ofDrawLine(p3.x, p3.y, p4.x, p4.y);
//    ofDrawLine(p23.x, p23.y, p4.x, p4.y);
//    ofDrawLine(p23.x, p23.y, p24.x, p24.y);
//
//    ofDrawLine(p22.x, p22.y,p5.x,  p5.y);
//    ofDrawLine(p22.x,  p22.y,p21.x, p21.y);
//    ofDrawLine(p21.x,p21.y, p6.x,  p6.y);
//    ofDrawLine(p6.x,  p6.y, p7.x,p7.y);
//    ofDrawLine(p7.x,p7.y, p19.x,  p19.y);
    
    ofMesh m1;
    ofMesh m2;
    ofMesh m3;
    ofMesh m4;
    ofMesh m5;
    ofMesh m6;
//    ofMesh m7;
//    ofMesh m8;
//    ofMesh m9;
//    ofMesh m10;


    m1.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m1.addVertex(ofPoint(p23));
    m1.addVertex(ofPoint(p4));
    m1.addVertex(ofPoint(p24));
    m1.addVertex(ofPoint(p3));


    m1.addTexCoord( img.getTexture().getCoordFromPoint(p15.x,p15.y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p9.x, p9.y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p16.x, p16.y));
    m1.addTexCoord( img.getTexture().getCoordFromPoint(p8.x, p8.y));

    m2.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m2.addVertex(ofPoint(p15));
    m2.addVertex(ofPoint(p9));
    m2.addVertex(ofPoint(p16));
    m2.addVertex(ofPoint(p8));


    m2.addTexCoord( img.getTexture().getCoordFromPoint(p23.x,p23.y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p4.x, p4.y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p24.x, p24.y));
    m2.addTexCoord( img.getTexture().getCoordFromPoint(p3.x, p3.y));
    
    
    m3.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m3.addVertex(ofPoint(p24));
    m3.addVertex(ofPoint(p2));
    m3.addVertex(ofPoint(p26));
    m3.addVertex(ofPoint(p1));


    m3.addTexCoord( img.getTexture().getCoordFromPoint(p12.x,p12.y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p10.x, p10.y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p14.x, p14.y));
    m3.addTexCoord( img.getTexture().getCoordFromPoint(p9.x, p9.y));

    m4.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    m4.addVertex(ofPoint(p12));
    m4.addVertex(ofPoint(p10));
    m4.addVertex(ofPoint(p14));
    m4.addVertex(ofPoint(p9));


    m4.addTexCoord( img.getTexture().getCoordFromPoint(p24.x,p24.y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p2.x, p2.y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p26.x, p26.y));
    m4.addTexCoord( img.getTexture().getCoordFromPoint(p1.x, p1.y));
    
    m5.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);

    m5.addVertex(ofPoint(p19));
    m5.addVertex(ofPoint(p7));
    m5.addVertex(ofPoint(p20));
    m5.addVertex(ofPoint(p6));


    m5.addTexCoord( img.getTexture().getCoordFromPoint(p21.x,p21.y));
    m5.addTexCoord( img.getTexture().getCoordFromPoint(p7p.x, p7p.y));
    m5.addTexCoord( img.getTexture().getCoordFromPoint(p22.x, p22.y));
    m5.addTexCoord( img.getTexture().getCoordFromPoint(p5.x, p5.y));

    m6.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);

    m6.addVertex(ofPoint(p20));
    m6.addVertex(ofPoint(p6));
    m6.addVertex(ofPoint(p22));
    m6.addVertex(ofPoint(p5));


    m6.addTexCoord( img.getTexture().getCoordFromPoint(p19.x,p19.y));
    m6.addTexCoord( img.getTexture().getCoordFromPoint(p7.x, p7.y));
    m6.addTexCoord( img.getTexture().getCoordFromPoint(p21.x, p21.y));
    m6.addTexCoord( img.getTexture().getCoordFromPoint(p7p.x, p7p.y));




    img.getTexture().bind();
    m1.draw();
    m2.draw();
    m3.draw();
    m4.draw();
    m5.draw();
    m6.draw();
    img.getTexture().unbind();


    
}

//--------------------------------------------------------------
